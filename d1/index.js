//this is a statement
console.log('Hello Batch 244!')

console. log ('Hello World!') ; 

console.
log

(
	"Hello Again!"
)

// [SECTION] Variables
// It is used to contain data 

//Declaring variable:Tells our device that a variable is created and is ready to store data.

//Syntax: let/const variableName;

let myVariable;
console.log(myVariable);  //Result: undefined

//Initializing variables
//Syntax: let/const variableName = value;

let productName = 'desktop computer';

console.log(productName)

let productPrice= 189999;
console.log(productPrice);

const interest = 3.539
console.log(interest);

// reassigning values
//Syntax: variableName=newValue;
productName='laptop'
console.log(productName)

let friend = 'Kate'
console.log(friend)
friend = 'jane'
console.log(friend)



let friend1='mark';
console.log(friend1);

/*Const variable can't be assigned again interest = 4.489;
console.log(interest);*/

/*let/const local/global scope */

let outerVariable = 'Hello';
{
	let innerVariable = 'Hello again';
	console.log(innerVariable);
}

console.log(outerVariable);

let productCode ='DC017';
const productBrand = 'Dell';
console.log(productCode, productBrand);

const greeting='Hi';
console.log(greeting);


//[SECTION] Data Types

	/* 
	Strings Strings are a series of characters that create a word, a phrase, a sentence or anything related to creating text
	Strings in JavaScript can be written using either a single (') or double (") quote
	*/

	let country='Philippines';
	let province='Batangas';
	console.log(country, province);
	// Concantenation
	let fullAddress= province + ',' + country;
	console.log(fullAddress);

	let greeting2= 'I live in the ' + country +".";
	console.log(greeting2);

	let message = 'John\'s employees went home early.';
	console.log(message);

	// Numbers
	// Integers/Whole Numbers
	let headCount =26;
	console.log(headCount);

	// Decimal Numbers
	let grade=98.7;
	console.log(grade);

	// Exponential Notation
	let planetDistance= 2e10;
	console.log(planetDistance);

	console.log('John\'s grade last quarter is ' + grade +"%");

	// Boolean -True or False
	let isMarried= false;
	let inGoodConduct= true;
	console.log('isMarried ' + isMarried);
	console.log('inGoodConduct ' + inGoodConduct);

	// Arrays
	// Syntax: arrayName = [e1,e2,e3,.....]

	let grades= [98,92,90,94];
	console.log(grades);

	let details= ['John','Smith',32,true];
	console.log(details);


	//Objects
	/*	Syntax: Let/const objectName=  {
				propertyA: value,
				propertyB: value,
	}
	 */

	 let person={
	 	fullName: "John Smith",
	 	age: 32,
	 	isMarried: true,
	 	contact:{"0912345678","0998989483"},
	 	address:{
	 		houseNumber: "1212",
	 		city: "Manila"}
	 }

	 console.log(person)

	 let (m)




























